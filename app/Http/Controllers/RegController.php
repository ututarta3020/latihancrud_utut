<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function show()
    {
        $karyawan = DB::table('karyawan')->get();

        return view('registrasi');
    }

    public function login()
    {
        return view('login');
    }

    public function store(Request $req)
    {
        $nama = $req->nama_karyawan;
        $nomor = $req->no_karyawan;
        $telp = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        DB::table('karyawan')->insert(
            ['nama_karyawan'=>$nama,'no_karyawan'=>$nomor,'no_telp_karyawan'=>$telp,'jabatan_karyawan'=>$jabatan,'divisi_karyawan'=>$divisi]
        );
    }
}
